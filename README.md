# TypeScript TODO List Example

> ✨ Bootstrapped with Create Snowpack App (CSA).

## References
* [Tutorial Video](https://www.youtube.com/watch?v=jBmrduvKl5w)
## Developer Notes
1. create directory
1. change to new directory
1. npm init -y
1. install typescript locally
1. tsc -init
## [Snowpack](https://www.snowpack.dev/tutorials/getting-started) Steps
1. npx create-snowpack-app . --template @snowpack/app-template-blank-typescript --force
1. [Deprecated tsconfig stuff](https://www.typescriptlang.org/docs/handbook/release-notes/typescript-5-0.html) under <em>Deprecations and Default Changes</em>.
## [WebPack](https://webpack.js.org/guides/getting-started/) Notes
1. npm i -D webpack webpack-cli
1. TBD
## Available Scripts

### npm start

Runs the app in the development mode.
Open http://localhost:8080 to view it in the browser.

The page will reload if you make edits.
You will also see any lint errors in the console.

### npm run build

Builds a static copy of your site to the `build/` folder.
Your app is ready to be deployed!

**For the best production performance:** Add a build bundler plugin like [@snowpack/plugin-webpack](https://github.com/snowpackjs/snowpack/tree/main/plugins/plugin-webpack) or [snowpack-plugin-rollup-bundle](https://github.com/ParamagicDev/snowpack-plugin-rollup-bundle) to your `snowpack.config.mjs` config file.

### Q: What about Eject?

No eject needed! Snowpack guarantees zero lock-in, and CSA strives for the same.
